package com.danit.java.basic.homework11.structure;

import com.danit.java.basic.homework11.domain.Family;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> familyList;

    public CollectionFamilyDao(List<Family> familyList) {
        this.familyList = familyList;
    }

@Override
    public void save(Family family) {
        familyList.add(family);
    }
@Override
   public  boolean deleteFamily(int index){
        if(index <  0 || index > familyList.size()-1){
            return false;
        }else{

        familyList.remove(index);

            return true;
        }
   }
   @Override
    public  boolean deleteFamily(Family family){
        if(family == null){
            return false;}
        int length = familyList.size();
        familyList.remove(family);
        if(familyList.size() < length){
            return true;
        }else return false;
    }
    public List<Family> findAll() {
        return familyList;
    }
@Override
    public Family getFamilyByIndex(int index){
        if(index <0 || index > familyList.size()){
            return  null;
        } else return familyList.get(index);
    }
    @Override
    public List <Family> getDataFromFile(){
        List <Family> families;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listFamily") ) ) {
            families = (List<Family>) objectInputStream.readObject();
            families.forEach(el -> el.prettyFormat());
            if(familyList.size() >0){
               familyList.clear();
            }
            families.forEach(el -> familyList.add(el));
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return  families;
    }
}
