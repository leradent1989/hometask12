package com.danit.java.basic.homework11.structure;

import com.danit.java.basic.homework11.domain.FamilyOverflowException;
import com.danit.java.basic.homework11.domain.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;

    }
public void loadData(List<Family> families){
        familyService.loadData(families);
}
    private void task1() {
        try {
            try {
                familyService.getDataFromFile();

            } catch (ClassCastException er) {
                er.printStackTrace();
                System.err.println("Не удалось загрузить данные");
            }


        } catch (NullPointerException er) {
            er.printStackTrace();
            System.err.println("Не удалось загрузить данные");
        }

    }

    private void task2() {
        try {
            if (familyService.getAllFamilies().size() == 0) {
                throw new Exception("LIST IS EMPTY PLEASE INSERT TEST DATA(MENU ITEM 1.)");
            }
            familyService.displayAllFamilies();

        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (Exception err) {
            err.printStackTrace();

        }

    }

    private void task3() {
        try {
            if (familyService.getAllFamilies() == null) {
                throw new NullPointerException();
            }
            System.out.println("Enter size");
            Scanner scan = new Scanner(System.in);
            int size = scan.nextInt();

            if (familyService.getAllFamilies().size() == 0) {
                throw new IndexOutOfBoundsException();
            }

            familyService.getFamiliesBiggerThen(size).forEach(el -> {
                System.out.println((familyService.getAllFamilies().indexOf(el) + 1));
                el.prettyFormat();
            });

        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (IndexOutOfBoundsException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.err.println("  Введите целое число ");
        }


    }


    private void task4() {
        try {
            if (familyService.getAllFamilies() == null) {
                throw new NullPointerException();
            }
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter size");
            int size2 = scan.nextInt();

            if (familyService.getAllFamilies().size() == 0) {
                throw new IndexOutOfBoundsException();
            }
            List<Family> list = familyService.getFamiliesLessThen(size2);
            list.forEach(el -> {
                System.out.println((familyService.getAllFamilies().indexOf(el) + 1));

                el.prettyFormat();
            });

        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (IndexOutOfBoundsException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (InputMismatchException e) {
            e.printStackTrace();
            System.err.println("  Введите целое число ");
        }

    }

    private void task5() {
        try {
            if (familyService.getAllFamilies().size() == 0) {
                throw new NullPointerException();
            }
            System.out.println("Enter size");
            Scanner scan = new Scanner(System.in);
            int size3 = scan.nextInt();
            if (familyService.getAllFamilies().size() == 0) {
                throw new IndexOutOfBoundsException();
            }
            System.out.println("Families with size " + size3 + " people : " + familyService.countFamiliesWithMemberNumber(size3));
        } catch (InputMismatchException err) {
            err.printStackTrace();
            System.err.println("  Введите целое число ");
        } catch (IndexOutOfBoundsException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        }

    }

    private void task6() {

        try {

            System.out.println("input mother name ");
            Scanner scan = new Scanner(System.in);
            String motherName = scan.nextLine();

            System.out.println("input mother surname ");
            String motherSurname = scan.nextLine();

            System.out.println("input mother birth date(dd/MM/YYYY ");

            String motherBirthDate = scan.nextLine();

            Date date = new SimpleDateFormat("dd/MM/yyyy").parse(motherBirthDate);
            long birthDateMother = date.getTime();

            System.out.println("Input father name");
            String fatherName = scan.nextLine();
            System.out.println("Input father surname");
            String fatherSurname = scan.nextLine();

            System.out.println("Input father birth date (dd/MM/YYYY)");

            String fatherBirthDate = scan.nextLine();
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(fatherBirthDate);
            long birthDateFather = date2.getTime();

            familyService.createNewFamily(new Human(motherName, motherSurname, (byte) 110, birthDateMother), new Human(fatherName, fatherSurname, (byte) 110, birthDateFather));
            familyService.displayAllFamilies();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException err) {
            err.printStackTrace();
        }

    }

    private void task7() {

        try {
            if (familyService.getAllFamilies().size() == 0) {
                throw new NullPointerException();
            }
            System.out.println("Please enter index of family");
            Scanner scan = new Scanner(System.in);
            int familyIndex = scan.nextInt();
            System.out.println("Family you are going to delete: \n");
            familyService.getFamilyById(familyIndex).prettyFormat();
            familyService.deleteFamilyByIndex(familyIndex);
            System.out.println("Family successfully deleted -there is no such family in list: \n ".toUpperCase(Locale.ROOT));
            familyService.displayAllFamilies();
        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        } catch (InputMismatchException e) {
            System.err.println("Index is too big");
            System.err.println("INVALID FORMAT Please try again\n");

        }
    }

    private void task8() {
        try {
            if (familyService.getAllFamilies().size() == 0) {
                throw new NullPointerException();
            }

            System.out.println("1.Born child");
            System.out.println("2.Adopt child");
            System.out.println("3.Return to main menu");
            System.out.println("Your choice:");
            Scanner scan = new Scanner(System.in);
            int menuChoice = scan.nextInt();
            switch (menuChoice) {
                case 1:
                    System.out.println("Please enter index  of family");
                    int familyIndex3 = scan.nextInt();
                    Family family = familyService.getFamilyById(familyIndex3);
                    if (family.countFamily(family.getChildren()) > 7) {
                        throw new FamilyOverflowException("Family size more then " + FamilyOverflowException.familySize);
                    }
                    scan.nextLine();
                    System.out.println("Please enter name if boy");
                    String boyName = scan.nextLine();

                    System.out.println("Please enter name if girl");

                    String girlName = scan.nextLine();
                    familyService.bornChild(family, girlName, boyName).prettyFormat();
                    break;
                case 2:
                    System.out.println("Please enter index  of family");
                    int familyIndex4 = scan.nextInt();
                    Family family2 = familyService.getFamilyById(familyIndex4);
                    System.out.println("Please enter child name");
                    scan.nextLine();
                    String adoptChildName = scan.nextLine();
                    System.out.println("Please enter child  surname ");
                    String adoptChildSurname = scan.nextLine();
                    System.out.println("Please enter child  birthDate(dd/MM/yyyy) ");
                    String adoptChildAge = scan.nextLine();
                    System.out.println("Please enter child  gender ");
                    String adoptChildGender = scan.nextLine();
                    System.out.println("Please enter child  iq (less then 100) ");
                    byte adoptChildIq = scan.nextByte();

                    familyService.adoptChild(family2, new Human(adoptChildName, adoptChildSurname, adoptChildAge, adoptChildIq, (adoptChildGender.equals("female") ? Gender.Female : Gender.Male))).prettyFormat();
                    break;
                case 3:
                    break;
            }
        } catch (FamilyOverflowException err) {
            err.printStackTrace();
        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        }

    }

    private void task9() {
        try {
            System.out.println("Enter child age");
            Scanner scan = new Scanner(System.in);
            int childAge = scan.nextInt();
            familyService.deleteAllChildrenOlderThen(childAge);
            familyService.displayAllFamilies();
        } catch (InputMismatchException err) {
            err.printStackTrace();
            System.err.println(" Введите целое число");
        } catch (NullPointerException e) {
            System.err.println("Please insert test data(menu item 1.) -list is empty \n".toUpperCase(Locale.ROOT));
            e.printStackTrace();

        }
    }

    public void start() throws ParseException {


        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Menu: ");
            System.out.println("1. Insert test data");
            System.out.println("2. Show all families ");
            System.out.println("3. Find families bigger then size ");
            System.out.println("4. Find families less then size");
            System.out.println("5. Count families with size");
            System.out.println("6. Create family");
            System.out.println("7. Delete family by index ");
            System.out.println("8. Edit family ");
            System.out.println("9. Delete children older then ");
            System.out.println("10. Exit");
            System.out.println("You choice: ");

            Scanner scanner = new Scanner(System.in);

            if (!scanner.hasNextInt()) {
                System.out.println("Illegal input");
               continue;
            }
            final Map commands = new HashMap();

            Runnable task1 = this::task1;
            Runnable task2 = this::task2;
            Runnable task3 = this::task3;
            Runnable task4 = this::task4;
            Runnable task5 = this::task5;
            Runnable task6 = this::task6;
            Runnable task7 = this::task7;
            Runnable task8 = this::task8;
            Runnable task9 = this::task9;
            Runnable task10 = () -> {
                familyService.getAllFamilies().forEach(el-> el.prettyFormat());
                this.loadData(familyService.getAllFamilies());
                System.exit(0);
            };
            List<Runnable> tasks = new ArrayList<>(List.of(task1, task2, task3, task4, task5, task6, task7, task8, task9, task10));

            tasks.forEach(el -> {

                commands.put(Integer.valueOf(tasks.indexOf(el) + 1), el);

            });


            int menuItem = scanner.nextInt();
            try {
                Runnable task = (Runnable) commands.get(Integer.valueOf(menuItem));
                task.run();
            } catch (Exception e) {
                System.out.println("Illegal input");
            }
        }
    }
}

