package com.danit.java.basic.homework11.domain;



import java.text.SimpleDateFormat;
import java.util.Date;

    public class AdoptedChild extends  Human{


        public AdoptedChild(String name, String surname, String birthDay,byte iq){
            super(name,surname,iq);

            try{
                Date date=new SimpleDateFormat("dd/MM/yyyy").parse(birthDay);
                this.birthDate = date.getTime();

            }catch(Exception e){
                System.out.println(e);
            }



        }

    }
