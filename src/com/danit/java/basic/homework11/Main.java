package com.danit.java.basic.homework11;

import com.danit.java.basic.homework11.domain.*;
import com.danit.java.basic.homework11.structure.CollectionFamilyDao;
import com.danit.java.basic.homework11.structure.FamilyController;
import com.danit.java.basic.homework11.structure.FamilyDao;
import com.danit.java.basic.homework11.structure.FamilyService;

import java.io.File;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.nio.file.Paths;
import java.nio.file.Path;

public class Main {

    public static void main(String[] args) throws ParseException {

        long millisInDay = 24 * 3600 * 1000;
        LocalDate dateLoc1 = LocalDate.of(1973, 10, 23);
        long date1 = dateLoc1.toEpochDay() * millisInDay;
        LocalDate dateLoc2 = LocalDate.of(1978, 9, 15);
        long date2 = dateLoc2.toEpochDay() * millisInDay;
        LocalDate dateLoc3 = LocalDate.of(2010, 3, 3);
        long date3 = dateLoc3.toEpochDay() * millisInDay;
        LocalDate dateLoc4 = LocalDate.of(2018, 8, 8);
        long date4 = dateLoc4.toEpochDay() * millisInDay;

        List<Family> familiesToFile = new ArrayList<>(List.of(
                new Family(new Human("gerge1", "grgr1", (byte) 110, date1), new Human("gdgd1", "dgdgd1", (byte) 110, date2)),
                new Family(new Human("gerge2", "grgr2", (byte) 110, date1), new Human("gdgd2", "dgdgd2", (byte) 110, date2)),
                new Family(new Human("gerge3", "grgr3", (byte) 110, date1), new Human("gdgd3", "dgdgd3", (byte) 110, date2)),
                new Family(new Human("gerge4", "grgr4", (byte) 110, date1), new Human("gdgd4", "dgdgd4", (byte) 110, date2)),
                new Family(new Human("gerge5", "grgr5", (byte) 110, date1), new Human("gdgd5", "dgdgd5", (byte) 110, date2)),
                new Family(new Human("gerge6", "grgr6", (byte) 110, date1), new Human("gdgd6", "dgdgd6", (byte) 110, date2))
        ));

        familiesToFile.get(0).getChildren().add(new Human("Jake", "vvtrt", "20/12/2008", (byte) 110, Gender.Male));
        familiesToFile.get(2).getChildren().add(new Human("Jane", "vvtrt", "20/12/2010", (byte) 110, Gender.Female));
        Set <Pet> pets = new HashSet<>(Set.of(new Dog("snoopy",4,(byte) 50),new Dog("linda",7,(byte) 60) ));



        List<Family> families = new ArrayList< >();

        FamilyDao familyDao = new CollectionFamilyDao(families);
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        // запись списка семей в файл только если файла еще не было  (в дальнейшем список записывается при выходе из цыкла (пункт 10) считывание  списка из файла другим методом в первом пункте менню )


        if (  !Paths.get("listFamily").toFile().exists()){
        familyController.loadData(familiesToFile);
    }

        // запуск приложения
        familyController.start();
        }


    }
